import React from 'react'
import '../../pages/style.css';
import { Container, Row, Col } from 'react-bootstrap'
import Text from './Text'
import {
  // AiFillGithub,
  // AiOutlineTwitter,
  AiOutlineFacebook,
  AiFillInstagram,
} from "react-icons/ai";
import { FaLinkedinIn } from "react-icons/fa";

function Home() {
  return (
    <div className='homepagebackground'>
      <Container>
        <Row>
          <Col md={7}>
            <h2 className='headtext'>Hello <span className='wave'>👋 </span></h2>
            <h2 className='nametext'>I'm AJAY</h2>
            <span></span>
            <Text />
            {/* <button onClick={() => {
              window.open("https://github.com/rahulvijay81");
            }}
              className='socailmediabtn'><AiFillGithub className='icon' /></button> */}
            <button onClick={() => {
              window.open("https://www.linkedin.com/in/ajay-r-b90857264/");
            }}
              className='socailmediabtn'><FaLinkedinIn className='icon' /></button>
            <button onClick={() => {
              window.open("https://www.facebook.com/profile.php?id=100011529104955");
            }}
              className='socailmediabtn'><AiOutlineFacebook className='icon' /></button>
            <button onClick={() => {
              window.open("https://www.instagram.com/aathiajayb/");
            }}
              className='socailmediabtn'><AiFillInstagram className='icon' /></button>
          </Col>

          <Col md={5}>
            <div className="imagedeveloper">
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Home;