import React from 'react';

const LetterA = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100">
    <defs>
      <linearGradient id="gradient" gradientTransform="rotate(45)">
        <stop offset="30%" stopColor="#007BFF" />
        <stop offset="50%" stopColor="#bbb656"/>
        <stop offset="100%" stopColor="#055425" />
      </linearGradient>
    </defs>
    <path d="M20 90L40 10H60L80 90H70L65 75H35L30 90H20Z" fill="url(#gradient)" />
    <polygon points="50 25 40 65 65 50 35 50 60 65" fill="#2fe176" />
  </svg>
  );
};

export default LetterA;
