import React from 'react'
import '../../pages/style.css';
import ProjectCard from "./ProjectCard";
import { Container, Row, Col } from 'react-bootstrap'
import useradminpanel from '../../Assets/CRUD.jpeg'
import olximage from '../../Assets/Lazy.jpg'
import netfliximage from '../../Assets/Redux.png'
import gridlinesbuilders from '../../Assets/Auth.jpg'
// import charlespizza from '../../Assets/Charlestown-Pizza.png'
// import todolist from '../../Assets/todolist.png'

function Projectlist() {
  return (
    <div className="projectbackground">
      <Container fluid className="project-section">
        <Container>
          <Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
            <Col md={4} className="project-card">
              <ProjectCard
                imgPath={useradminpanel}
                isBlog={false}
                title="CRUD Operation"
                description="
                  
                  Create (C): The Create operation involves adding new data to the database. It typically includes inserting a new record or entity into a table or collection.

Read (R): The Read operation involves retrieving data from the database. It includes querying the database to fetch specific records or retrieving all records from a table or collection.

Update (U): The Update operation involves modifying existing data in the database. It includes changing the values of one or more fields within a record or updating multiple records that match certain criteria.

Delete (D): The Delete operation involves removing data from the database. It includes deleting a specific record or removing multiple records that meet certain conditions.
                  "
                // ghLink="https://rahul-dashboard-pannel.netlify.app"
              />
            </Col>

            <Col md={4} className="project-card">
              <ProjectCard
                imgPath={olximage}
                isBlog={false}
                title="Lazy Loading"
                description="Lazy loading is a technique used in software development to optimize the loading and rendering of content or components. 
                It is particularly beneficial when dealing with large or complex applications where loading 
                everything upfront can result in performance issues.
                Lazy loading helps optimize performance by reducing the initial loading time, 
                minimizing resource usage, and improving the overall user experience.
                "
                // ghLink="https://olx-clone-rahul.netlify.app/"
              />
            </Col>

            <Col md={4} className="project-card">
              <ProjectCard
                imgPath={netfliximage}
                isBlog={false}
                title="Redux"
                description="Redux follows a unidirectional data flow pattern and helps 
                in managing the state of an application in a predictable manner. 
                It separates the state from the components and stores it in a central store.
                 React Redux acts as a bridge between Redux and React, allowing components to access and update the state from the store.
                 React Redux simplifies state management in React applications, 
                 promotes code organization, and provides a scalable architecture for building complex and maintainable applications."
                
                 // ghLink="https://react-netflixweb-clone.netlify.app/"
              />
            </Col>

            <Col md={4} className="project-card">
              <ProjectCard
                imgPath={gridlinesbuilders}
                isBlog={false}
                title="Authentication"
                description="Authentication is the process of verifying the identity of a user or entity.
                 In web applications, authentication is commonly used to allow access to certain resources 
                 or functionalities based on the user's identity.
                 It is crucial to implement secure authentication practices to protect user information and prevent unauthorized access. This includes techniques like using secure protocols
                 "
                // ghLink="https://gridlinesbuilders.in/"
              />
            </Col>

            {/* <Col md={4} className="project-card">
              <ProjectCard
                imgPath={charlespizza}
                isBlog={false}
                title="Charlestown Pizza"
                description="Charlestown pizza is a static website completely build with bootstrap with fully responsive."
                ghLink="https://rahulvijay81.github.io/Pizzastore-bootstrap/"
              />
            </Col>

            <Col md={4} className="project-card">
              <ProjectCard
                imgPath={todolist}
                isBlog={false}
                title="To Do List"
                description=" To-Do App that build will allow a user to add a task to a list of to-do items. Once the task is added, the user will be able to delete it as completed once it has done."
                ghLink="https://todolist-rahul81.netlify.app/"

              />
            </Col> */}
          </Row>
        </Container>
      </Container>
    </div>
  )
}
export default Projectlist